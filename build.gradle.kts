plugins {
    id(GradlePluginId.KTLINT) version GradlePluginVersion.KTLINT
    id(GradlePluginId.DETEKT) version GradlePluginVersion.DETEKT
}

buildscript {
    repositories {
        google()
        jcenter()
    }
    dependencies {
        classpath("com.android.tools.build:gradle:${GradlePluginVersion.ANDROID_GRADLE}")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${GradlePluginVersion.KOTLIN}")
    }
}

allprojects {
    repositories {
        google()
        jcenter()
    }
}

tasks {
    register("clean", Delete::class) {
        delete(rootProject.buildDir)
    }
    withType<io.gitlab.arturbosch.detekt.Detekt> {
        this.jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}
task("staticCheck") {
    afterEvaluate {
        dependsOn(listOf("ktlintCheck", "detekt", "app:lint"))
    }
}
