private object LibraryVersion {
    const val ANDROID_CORE_KTX = "1.3.2"
    const val APPCOMPAT = "1.2.0"
    const val MATERIAL = "1.2.1"
    const val CONSTRAINT_LAYOUT = "2.0.4"
    const val NAVIGATION = "2.3.2"
    const val CARD_IO = "5.5.1"
}

object LibraryDependency {
    const val KOTLIN =
        "org.jetbrains.kotlin:kotlin-stdlib:${CoreVersion.KOTLIN}"
    const val ANDROID_CORE_KTX =
        "androidx.core:core-ktx:${LibraryVersion.ANDROID_CORE_KTX}"
    const val APPCOMPAT =
        "androidx.appcompat:appcompat:${LibraryVersion.APPCOMPAT}"
    const val MATERIAL =
        "com.google.android.material:material:${LibraryVersion.MATERIAL}"
    const val CONSTRAINT_LAYOUT =
        "androidx.constraintlayout:constraintlayout:${LibraryVersion.CONSTRAINT_LAYOUT}"
    const val NAV_FRAGMENT =
        "androidx.navigation:navigation-fragment-ktx:${LibraryVersion.NAVIGATION}"
    const val NAV_UI =
        "androidx.navigation:navigation-ui-ktx:${LibraryVersion.NAVIGATION}"
    const val CARD_IO =
        "io.card:android-sdk:${LibraryVersion.CARD_IO}"
}