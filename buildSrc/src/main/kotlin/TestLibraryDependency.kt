private object TestLibraryVersion {
    const val JUNIT = "4.13.1"
    const val ANDROID_JUNIT_EXT = "1.1.2"
    const val ESPRESSO_CORE = "3.3.0"
    const val MOCKK = "1.10.3-jdk8"
    const val FLUENT = "1.64"
    const val ANDROID_ARCH = "2.1.0"
}

object TestLibraryDependency {
    const val JUNIT =
        "junit:junit:${TestLibraryVersion.JUNIT}"
    const val ANDROID_JUNIT_EXT =
        "androidx.test.ext:junit:${TestLibraryVersion.ANDROID_JUNIT_EXT}"
    const val ESPRESSO_CORE =
        "androidx.test.espresso:espresso-core:${TestLibraryVersion.ESPRESSO_CORE}"
    const val MOCKK =
        "io.mockk:mockk:${TestLibraryVersion.MOCKK}"
    const val FLUENT =
        "org.amshove.kluent:kluent-android:${TestLibraryVersion.FLUENT}"
    const val ARCH_CORE_TESTING =
        "androidx.arch.core:core-testing:${TestLibraryVersion.ANDROID_ARCH}"
}