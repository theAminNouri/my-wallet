object GradlePluginVersion {
    const val ANDROID_GRADLE = "4.1.1"
    const val KOTLIN = CoreVersion.KOTLIN
    const val KTLINT= "9.4.1"
    const val DETEKT = "1.15.0-RC1"
}

object GradlePluginId {
    const val ANDROID_APPLICATION = "com.android.application"
    const val KOTLIN_ANDROID = "kotlin-android"
    const val KTLINT = "org.jlleitschuh.gradle.ktlint"
    const val DETEKT = "io.gitlab.arturbosch.detekt"
}

