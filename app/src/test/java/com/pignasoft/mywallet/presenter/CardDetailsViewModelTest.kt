package com.pignasoft.mywallet.presenter

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.pignasoft.mywallet.presenter.carddetails.CardDetailsViewModel
import com.pignasoft.mywallet.presenter.carddetails.state.CardDetailsViewState
import com.pignasoft.mywallet.presenter.carddetails.state.StateEvent
import io.mockk.CapturingSlot
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.SpyK
import io.mockk.verify
import org.amshove.kluent.shouldBeEqualTo
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class CardDetailsViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var cut: CardDetailsViewModel

    @SpyK
    var testObserver = Observer<CardDetailsViewState>({})

    private val defaultViewState = CardDetailsViewState()

    @Before
    fun before() {
        MockKAnnotations.init(this)
        cut = CardDetailsViewModel(defaultViewState)
        cut.viewState.observeForever(testObserver)
    }

    @Test
    fun whenCreateViewModel_setDefaultViewState() {
        val slot = CapturingSlot<CardDetailsViewState>()

        verify { testObserver.onChanged(capture(slot)) }

        slot.captured shouldBeEqualTo defaultViewState
    }

    @Test
    fun whenScanResultStateEvent_setNewViewState() {
        val newViewState = CardDetailsViewState(
            number = "1234 5678 9122 2222",
            type = "MasterCard",
            expiryMonth = "10",
            expiryYear = "2021",
            cvv = "1234"
        )

        cut.setStateEvent(
            StateEvent.ScanResultReceivedStateEvent(
                cardNumber = "1234 5678 9122 2222",
                cardType = "MasterCard",
                expiryMonth = "10",
                expiryYear = "2021",
                cvv = "1234"
            )
        )

        val list = mutableListOf<CardDetailsViewState>()

        verify { testObserver.onChanged(capture(list)) }

        list.first() shouldBeEqualTo defaultViewState
        list.last() shouldBeEqualTo newViewState
    }

    @Test
    fun whenExpiryDateStateEvent_setNewViewState() {
        val newViewState = defaultViewState.copy(
            expiryMonth = "12",
            expiryYear = "2022"
        )

        cut.setStateEvent(
            StateEvent.ExpiryDateChangedStateEvent(
                expiryMonth = "12",
                expiryYear = "2022"
            )
        )

        val list = mutableListOf<CardDetailsViewState>()

        verify { testObserver.onChanged(capture(list)) }

        list.first() shouldBeEqualTo defaultViewState
        list.last() shouldBeEqualTo newViewState
    }


}