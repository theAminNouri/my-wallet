package com.pignasoft.mywallet.presenter.carddetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pignasoft.mywallet.presenter.carddetails.state.CardDetailsViewState
import com.pignasoft.mywallet.presenter.carddetails.state.StateEvent

class CardDetailsViewModel(private val viewStateInitial: CardDetailsViewState = CardDetailsViewState()) :
    ViewModel() {
    private val _viewState = MutableLiveData<CardDetailsViewState>()
    val viewState: LiveData<CardDetailsViewState>
        get() = _viewState

    init {
        setViewState(getCurrentViewState())
    }

    private fun updateViewState(newViewState: CardDetailsViewState) {
        val update = getCurrentViewState().copy()
        newViewState.number?.let {
            update.number = it
        }
        newViewState.type?.let {
            update.type = it
        }
        newViewState.expiryMonth?.let {
            update.expiryMonth = it
        }
        newViewState.expiryYear?.let {
            update.expiryYear = it
        }
        newViewState.cvv?.let {
            update.cvv = it
        }
        setViewState(update)
    }

    private fun setViewState(viewState: CardDetailsViewState) {
        if (_viewState.value != viewState)
            _viewState.value = viewState
    }

    private fun getCurrentViewState(): CardDetailsViewState {
        return _viewState.value ?: viewStateInitial
    }

    fun setStateEvent(stateEvent: StateEvent) {
        when (stateEvent) {
            is StateEvent.ScanResultReceivedStateEvent ->
                updateViewState(
                    CardDetailsViewState(
                        number = stateEvent.cardNumber,
                        type = stateEvent.cardType,
                        expiryMonth = stateEvent.expiryMonth,
                        expiryYear = stateEvent.expiryYear,
                        cvv = stateEvent.cvv
                    )
                )
            is StateEvent.ExpiryDateChangedStateEvent -> updateViewState(
                CardDetailsViewState(
                    expiryMonth = stateEvent.expiryMonth,
                    expiryYear = stateEvent.expiryYear,
                )
            )
        }
    }

}