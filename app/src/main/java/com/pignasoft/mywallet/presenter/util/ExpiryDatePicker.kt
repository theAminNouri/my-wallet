package com.pignasoft.mywallet.presenter.util

import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.view.View
import com.pignasoft.mywallet.R
import java.util.*

class ExpiryDatePicker(
    context: Context,
    listener: OnDateSetListener,
    year: Int = Calendar.getInstance().get(Calendar.YEAR),
    month: Int = Calendar.getInstance().get(Calendar.MONTH)
) :
    DatePickerDialog(
        context,
        R.style.MySpinnerDatePickerStyle,
        listener,
        year,
        month,
        0
    ) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val findViewById = datePicker.findViewById<View>(
            context.resources.getIdentifier(
                "day",
                "id",
                "android"
            )
        )
        findViewById?.visibility = View.GONE
    }
}
