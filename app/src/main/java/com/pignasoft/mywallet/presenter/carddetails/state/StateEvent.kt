package com.pignasoft.mywallet.presenter.carddetails.state

sealed class StateEvent {
    data class ScanResultReceivedStateEvent(
        val cardNumber: String?,
        val cardType: String?,
        val cvv: String?,
        val expiryMonth: String?,
        val expiryYear: String?
    ) : StateEvent()

    data class ExpiryDateChangedStateEvent(
        val expiryMonth: String?,
        val expiryYear: String?
    ) : StateEvent()
}