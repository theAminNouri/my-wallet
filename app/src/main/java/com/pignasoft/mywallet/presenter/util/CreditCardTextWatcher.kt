package com.pignasoft.mywallet.presenter.util

import android.text.Editable
import android.text.TextWatcher

class CreditCardTextWatcher : TextWatcher {
    private val TOTAL_SYMBOLS = 19
    private val TOTAL_DIGITS = 16
    private val DIVIDER_MODULO = 5
    private val DIVIDER_POSITION = DIVIDER_MODULO - 1
    private val DIVIDER = ' '
    private val strBuilder = StringBuilder()
    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

    }

    override fun afterTextChanged(s: Editable) {
        if ((s.isNotEmpty()) && !isInputCorrect(s, TOTAL_SYMBOLS, DIVIDER_MODULO, DIVIDER)) {
            val buildCorrectString = buildCorrectString(
                getDigitArray(s, TOTAL_DIGITS),
                DIVIDER_POSITION,
                DIVIDER
            )
            s.replace(0, s.length, buildCorrectString)
        }
    }

    private fun isInputCorrect(
        s: Editable,
        totalSymbols: Int,
        dividerModulo: Int,
        divider: Char
    ): Boolean {
        var isCorrect = s.length <= totalSymbols // check size of entered string
        for (i in s.indices) { // check that every element is right
            isCorrect = if (i > 0 && (i + 1) % dividerModulo == 0) {
                isCorrect and (divider == s[i])
            } else {
                isCorrect and Character.isDigit(s[i])
            }
        }
        return isCorrect
    }

    private fun buildCorrectString(
        digits: CharArray,
        dividerPosition: Int,
        divider: Char
    ): String {
        strBuilder.clear()
        val formatted = strBuilder
        for (i in digits.indices) {
            if (digits[i].toInt() != 0) {
                formatted.append(digits[i])
                if (i > 0 && i < digits.size - 1 && (i + 1) % dividerPosition == 0) {
                    formatted.append(divider)
                }
            }
        }
        return formatted.toString()
    }

    private fun getDigitArray(s: Editable, size: Int): CharArray {
        val digits = CharArray(size)
        var index = 0
        var i = 0
        while (i < s.length && index < size) {
            val current = s[i]
            if (Character.isDigit(current)) {
                digits[index] = current
                index++
            }
            i++
        }
        return digits
    }

}