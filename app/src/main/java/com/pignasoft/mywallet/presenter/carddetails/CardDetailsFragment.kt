package com.pignasoft.mywallet.presenter.carddetails

import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.pignasoft.mywallet.R
import com.pignasoft.mywallet.presenter.carddetails.state.CardDetailsViewState
import com.pignasoft.mywallet.presenter.carddetails.state.StateEvent
import com.pignasoft.mywallet.presenter.util.CreditCardTextWatcher
import com.pignasoft.mywallet.presenter.util.ExpiryDatePicker
import io.card.payment.CardIOActivity
import io.card.payment.CardType
import io.card.payment.CreditCard


class CardDetailsFragment : Fragment(), DatePickerDialog.OnDateSetListener {

    private val viewModel: CardDetailsViewModel by viewModels { ViewModelProvider.NewInstanceFactory() }
    private var cardCapture: AppCompatImageView? = null
    private var cardNumber: AppCompatEditText? = null
    private var cardIcon: AppCompatImageView? = null
    private var cvv: AppCompatEditText? = null
    private var expiryDate: AppCompatTextView? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_card_details, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cardCapture = view.findViewById(R.id.card_capture)
        cardCapture?.setOnClickListener { onScanPress() }
        cardNumber = view.findViewById(R.id.card_number)
        cardNumber?.addTextChangedListener(CreditCardTextWatcher())
        cardIcon = view.findViewById(R.id.card_icon)
        cvv = view.findViewById(R.id.cvv)
        expiryDate = view.findViewById(R.id.expiry_date)
        view.findViewById<View>(R.id.expiry_date_root)?.setOnClickListener {
            val dialog = ExpiryDatePicker(requireContext(), this)
            dialog.show()
        }
        subscribe()
    }

    private fun subscribe() {
        viewModel.viewState.observe(viewLifecycleOwner) { viewState ->
            render(viewState)
        }
    }

    private fun render(viewState: CardDetailsViewState?) {
        viewState?.let {
            viewState.number?.let {
                cardNumber?.setText(it)
            }
            viewState.type?.let {
                cardIcon?.setImageBitmap(getCardIconBitmap(it))
            }
            viewState.expiryYear?.let {
                expiryDate?.text = viewState.formattedExpiryDate()
                expiryDate?.setTextColor(requireContext().resources.getColor(R.color.black))
            }
            viewState.cvv?.let {
                cvv?.setText(it)
            }
        }
    }

    private fun getCardIconBitmap(type: String): Bitmap? {
        val validType = if (type.isEmpty()) CardType.UNKNOWN.toString() else type
        val cardType = CardType.valueOf(validType.toUpperCase())

        return if (cardType != CardType.UNKNOWN)
            cardType.imageBitmap(context)
        else
            BitmapFactory.decodeResource(resources, R.drawable.ic_round_credit_card_24)
    }

    private fun onScanPress() {
        val scanIntent = Intent(requireContext(), CardIOActivity::class.java).apply {
            putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, false)
            putExtra(CardIOActivity.EXTRA_REQUIRE_CARDHOLDER_NAME, false)
            putExtra(CardIOActivity.EXTRA_SUPPRESS_MANUAL_ENTRY, true)
            putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, false)
            putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false)
            putExtra(CardIOActivity.EXTRA_HIDE_CARDIO_LOGO, true)
            putExtra(CardIOActivity.EXTRA_SUPPRESS_CONFIRMATION, true)
            putExtra(CardIOActivity.EXTRA_USE_PAYPAL_ACTIONBAR_ICON, false)
        }
        startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MY_SCAN_REQUEST_CODE) {
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                val scanResult: CreditCard? =
                    data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT)

                setCardDetails(scanResult)
            }
        }
    }

    private fun setCardDetails(scanResult: CreditCard?) {
        viewModel.setStateEvent(
            StateEvent.ScanResultReceivedStateEvent(
                cardNumber = scanResult?.cardNumber,
                cardType = scanResult?.cardType.toString(),
                cvv = scanResult?.cvv,
                expiryMonth = if (scanResult?.isExpiryValid == true) scanResult.expiryMonth.toString() else null,
                expiryYear = if (scanResult?.isExpiryValid == true) scanResult.expiryYear.toString() else null
            )
        )
    }

    override fun onDateSet(datePicker: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        viewModel.setStateEvent(
            StateEvent.ExpiryDateChangedStateEvent(
                expiryMonth = (monthOfYear + 1).toString(),
                expiryYear = year.toString()
            )
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        cardCapture = null
        cardNumber = null
        cardIcon = null
        expiryDate = null
    }

    companion object {
        const val MY_SCAN_REQUEST_CODE = 100
    }
}
