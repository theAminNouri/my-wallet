package com.pignasoft.mywallet.presenter.carddetails.state

interface ViewState

data class CardDetailsViewState(
    var number: String? = null,
    var type: String? = null,
    var expiryMonth: String? = null,
    var expiryYear: String? = null,
    var cvv: String? = null
) : ViewState{

    fun formattedExpiryDate() =
        "${expiryMonth}/${expiryYear}"
}
